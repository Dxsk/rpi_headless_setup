#!/bin/bash

echo "Detection of required binaries..."

if ! command -v docker &> /dev/null
then
    echo "Error: Docker Not found !"
    echo "Install docker --> 'https://docs.docker.com/engine/install/'"
    exit 1
fi

echo "==> Docker found !"

if ! command -v docker-compose &> /dev/null
then
    echo "Error: Docker Compose Not found !"
    echo "Install docker-compose --> 'https://docs.docker.com/compose/install/'"
    exit 1
fi

echo "==> Docker Compose found !"



