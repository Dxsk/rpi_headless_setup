#!/bin/bash

iptables="/usr/sbin/iptables"
sysctl="/usr/sbin/sysctl"

echo 'disable ipv4 forward'
sudo su -c "echo 'net.ipv4.ip_forward=0' > /etc/sysctl.conf"
echo 'apply ipv4 forward'
sudo $sysctl -p
echo 'remove rules iptables'
sudo $iptables -t nat -D POSTROUTING -s 10.0.1.0/24 -j  MASQUERADE
sudo $iptables -t nat -D POSTROUTING -d 10.0.1.0/24 -j  MASQUERADE
sudo $iptables -P FORWARD DROP
echo 'done'
