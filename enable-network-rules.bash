#!/bin/bash

iptables="/usr/sbin/iptables"
sysctl="/usr/sbin/sysctl"

echo 'enable ipv4 forward'
sudo su -c "echo 'net.ipv4.ip_forward=1' > /etc/sysctl.conf"
echo 'apply ipv4 forward'
sudo $sysctl -p
echo 'add rules iptables'
sudo $iptables -t nat -A POSTROUTING -s 10.0.1.0/24 -j  MASQUERADE
sudo $iptables -t nat -A POSTROUTING -d 10.0.1.0/24 -j  MASQUERADE
sudo $iptables -P FORWARD ACCEPT
echo 'done'
