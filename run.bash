#!/bin/bash

./detection.bash

if [ ! $? == 0 ]
then
    exit 1
fi

./enable-network-rules.bash
./start-dhcpd.bash
./disabled-network-rules.bash
